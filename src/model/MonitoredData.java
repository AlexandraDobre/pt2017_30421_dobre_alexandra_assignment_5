package model;

import java.util.Date;

public class MonitoredData {
    private Date startDate;
    private Date endDate;
    private String activityLabel;

    public MonitoredData(Date startDate, Date endDate, String activityLabel) {
        this.startDate = startDate;
        this.endDate = endDate;
        this.activityLabel = activityLabel;
    }

    public Long getDurationInMillies() {
        return (getEndDate().getTime() - getStartDate().getTime());
    }

    public Date getStartDate() {
        return this.startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return this.endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getActivityLabel() {
        return this.activityLabel;
    }

    public void setActivityLabel(String activityLabel) {
        this.activityLabel = activityLabel;
    }
}
