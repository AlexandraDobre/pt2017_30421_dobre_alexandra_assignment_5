package processing;


import model.MonitoredData;
import utils.Constants;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class MonitoredDataInOut {

    public List<MonitoredData> getMonitoredDataFromFile(String fileName) {
        List<MonitoredData> monitoredDataTuplesList = new ArrayList<>();
        List<String> monitoredDataStringList = new ArrayList<>();

        try (BufferedReader br = Files.newBufferedReader(Paths.get(fileName))) {

            // Convert the lines into a List of strings, monitoredDataStringList
            monitoredDataStringList = br
                    .lines()
                    .collect(Collectors.toList());

            for (String monitoredDataString : monitoredDataStringList) {
                MonitoredData monitoredDataObject = createMonitoredDataObject(monitoredDataString);
                monitoredDataTuplesList.add(monitoredDataObject);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        return monitoredDataTuplesList;
    }

    // write into the given file, using a predefined separator
    public void writeMapToFile(String fileName, Map map) {
        try(Writer writer = Files.newBufferedWriter(Paths.get(fileName))) {
            map.forEach((key, value) -> {
                try {
                    writer.write(key + Constants.MAP_SEPARATOR + value + System.lineSeparator());
                } catch (IOException ex) {
                    throw new UncheckedIOException(ex);
                }
            });
        } catch (IOException e) {
            e.getStackTrace();
        }
    }

    // write a list into a file, one line after another
    public void writeListToFile(String fileName, List<String> list) {
        try(Writer writer = Files.newBufferedWriter(Paths.get(fileName))) {
            list.forEach((listElement) -> {
                try {
                    writer.write(listElement + System.lineSeparator());
                } catch (IOException ex) {
                    throw new UncheckedIOException(ex);
                }
            });
        } catch (IOException e) {
            e.getStackTrace();
        }
    }


    private MonitoredData createMonitoredDataObject(String monitoredDataString) {
    	//parse each string by considering distinct elements separated by at least 2 spaces
        // we need to use trim to eliminate the spaces at the beginning or at the end of an element
        List<String> monitoredDataElements = Stream
                .of(monitoredDataString)
                .map(element -> element.split("\\s{2,}")) 
                .flatMap(Arrays::stream)
                .map(String::trim)  
                .collect(Collectors.toList());

        Date startDate = null;
        Date endDate = null;
        try {
            startDate = new SimpleDateFormat(Constants.DATE_FORMAT, Locale.ENGLISH).parse(monitoredDataElements.get(0));
            endDate = new SimpleDateFormat(Constants.DATE_FORMAT, Locale.ENGLISH).parse(monitoredDataElements.get(1));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        String activityLabel = monitoredDataElements.get(2);

        MonitoredData monitoredDataObject = new MonitoredData(startDate, endDate, activityLabel);
        return monitoredDataObject;
    }
}
