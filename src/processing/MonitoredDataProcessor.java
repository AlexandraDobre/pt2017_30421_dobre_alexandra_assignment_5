package processing;


import model.MonitoredData;
import utils.Constants;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.*;
import java.util.stream.Collectors;

public class MonitoredDataProcessor {

    private String fileName;
    private List<MonitoredData> monitoredData;

    public MonitoredDataProcessor(String fileName) {
        this.fileName = fileName; //we use this because we have the same parameter name as declared above
    }

    public void importMonitoredData() {
        MonitoredDataInOut monitoredDataImporter = new MonitoredDataInOut();
        this.monitoredData = monitoredDataImporter.getMonitoredDataFromFile(fileName);
    }

    public int countDistinctDays() {
    	//use a set of strings as data type because the elements in a set are unique so we add an element only once
        //we only need the date so this is why we use only the date and not the time
        Set<String> distinctDays = new HashSet<>();
        DateFormat formatter = new SimpleDateFormat(Constants.DATE_FORMAT_WITHOUT_TIME);

        for (MonitoredData monitoredDataObject : this.monitoredData) { 
        	//we count both startDate and endTime
            distinctDays.add(formatter.format(monitoredDataObject.getStartDate()));
            distinctDays.add(formatter.format(monitoredDataObject.getEndDate()));
        }

        return distinctDays.size();
    }

    public Map<String,Long> determineOccurrencesForActivities() { 
    	//the data from the stream is put in the map grouped by the activityLabel field in order to obtain the occurrence no. for each activity
    	Map<String, Long> result = monitoredData.stream()
                .collect(Collectors.groupingBy(MonitoredData::getActivityLabel, Collectors.counting()));

        return result;
    }

    public Map<Integer, Map<String, Long>> getActivitiesOccurrencesPerEachDay() {
    	// we use TreeMap to automatically sort by ascending alfabetical order
        Map<Integer, Map<String, Long>> result = new TreeMap<>(); 

        // first, count the distinct days
        // we use TreeSet for the initialization of the set which has the distinct days because the days should also be kept in a ascending order
        Set<String> distinctDays = new TreeSet<>(); //string by default 
        DateFormat formatter = new SimpleDateFormat(Constants.DATE_FORMAT_WITHOUT_TIME); //date becomes a string

        for (MonitoredData monitoredDataObject : this.monitoredData) {
            distinctDays.add(formatter.format(monitoredDataObject.getStartDate()));
            distinctDays.add(formatter.format(monitoredDataObject.getEndDate()));
        }

        //iterate by distinct days and for each day we determine the activities and their occurrence number in that day
        //we need both start and end time
        //in result the days are kept with an order number; for this we use dayNumber
        int dayNumber = 0;
        for (String distinctDay : distinctDays) {
            dayNumber = dayNumber + 1;
            Map<String, Long> dayResult = monitoredData.stream()
                    .filter(monitoredDataObject -> formatter.format(monitoredDataObject.getStartDate()).equals(distinctDay)
                            || formatter.format(monitoredDataObject.getEndDate()).equals(distinctDay))
                    .collect(Collectors.groupingBy(MonitoredData::getActivityLabel, Collectors.counting()));

            result.put(dayNumber, dayResult);
        }

        return result;
    }

    public Map<String, LocalDateTime> determineLargeDurationActivities() {
        // at first we get the total duration in millis by grouping the stream by the activityLabel
        // the key from the map will sum the duration in millis of the activityLabel
        Map<String, Long> intermediateResult = monitoredData.stream()
                .collect(Collectors.groupingBy(MonitoredData::getActivityLabel, Collectors.summingLong(MonitoredData::getDurationInMillies)));

        //determine the entries of which total duration exceeds 10h = 36000000millis( 10 * 3600 * 1000)
        Map<String, LocalDateTime> result = intermediateResult.entrySet().stream()
                .filter(map -> map.getValue() >= 36000000) //10h = 10 * 60min * 60 000 milisec
                .collect(Collectors.toMap(p -> p.getKey(), p -> LocalDateTime.ofInstant(Instant.ofEpochMilli(p.getValue()), ZoneId.systemDefault()) ));

        return result;
    }

    public List<String> determineLowDurationActivities() {
        List<String> result = new ArrayList<>();
        //first determine the total number of occurrences of each activityLabel
        Map<String, Long> numberOfOccurrencesMap = monitoredData.stream()
                .collect(Collectors.groupingBy(MonitoredData::getActivityLabel, Collectors.counting()));
        
        //determine the map containing the number of occurrences of activities whose duration < 5min
        Map<String, Long> lowDurationOccurrencesMap = monitoredData.stream()
                .filter(activity -> activity.getDurationInMillies() < 300000)
                .collect(Collectors.groupingBy(MonitoredData::getActivityLabel, Collectors.counting()));
        
        //iterate over the first activity map and determine those who appear more than 90%
        for (Map.Entry<String, Long> entry : numberOfOccurrencesMap.entrySet()) {
            Double lowDurationOccurrences = entry.getValue() * 0.9;
            Long key = lowDurationOccurrencesMap.get(entry.getKey());
            if (key != null && lowDurationOccurrences <= key.doubleValue()) {
                result.add(entry.getKey());
            }
        }

        return result;
    }

    public List<MonitoredData> getMonitoredData() {
        return this.monitoredData;
    }

    public void setMonitoredData(List<MonitoredData> monitoredData) {
        this.monitoredData = monitoredData;
    }

    public static void main(String[] args) {
        // At first we need to import the data. 
    	//For this we will create the processor object and call the importMonitoredDate method
        MonitoredDataInOut monitoredDataExporter = new MonitoredDataInOut();
        MonitoredDataProcessor monitoredDataProcessor = new MonitoredDataProcessor("C:/Activities.txt");
        monitoredDataProcessor.importMonitoredData();

        // 1
        int distinctDays = monitoredDataProcessor.countDistinctDays();
        System.out.println("The number of distinct days is " + distinctDays);

        // 2
        Map<String, Long> occurencesForActivities = monitoredDataProcessor.determineOccurrencesForActivities();
        System.out.println("Map with occurences for activities determined!");
        monitoredDataExporter.writeMapToFile("C:/2", occurencesForActivities);

        // 3
        Map<Integer, Map<String, Long>> occurrencesPerEachDay = monitoredDataProcessor.getActivitiesOccurrencesPerEachDay();
        monitoredDataExporter.writeMapToFile("C:/3", occurrencesPerEachDay);
        System.out.println("Map with occurrences of activities per each day determined!");

        // 4
        Map<String, LocalDateTime> totalDurationPerActivity = monitoredDataProcessor.determineLargeDurationActivities();
        monitoredDataExporter.writeMapToFile("C:/4", totalDurationPerActivity);
        System.out.println("Map with total duration per activity that took more then 10h determined!");

        // 5
        List<String> lowDurationActivities = monitoredDataProcessor.determineLowDurationActivities();
        monitoredDataExporter.writeListToFile("C:/5", lowDurationActivities);
        System.out.println("Low duration activities (for which 90% of the monitoring samples have the duration < 5min) determined!");
    }

}
